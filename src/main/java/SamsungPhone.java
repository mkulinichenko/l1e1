public class SamsungPhone extends Phone implements PhoneConnection, PhoneMedia{

    private int cameraExtension;
    public int getCameraExtension(){
        return cameraExtension;
    }
    public void setCameraExtension(int cameraExtension) {
        this.cameraExtension = cameraExtension;
    }
    public SamsungPhone(String name, String model, int storageVolume, int ram, int cameraExtension) {
        super(name, model, storageVolume, ram);
        this.cameraExtension = cameraExtension;
    }

    @Override
    public void sendMessage() {
        System.out.println("Відправлення повідомлення...");
    }

    @Override
    public void call() {
        System.out.println("Вихідний виклик...");
    }
    @Override
    public void takePhoto() {
        System.out.println("Зробити фото!");
    }

    @Override
    public void takeVideo() {
        System.out.println("Зняти відео!");
    }
    @Override
    public String toString(){return getName() +" - "+ getModel() +" - "+ getStorageVolume() +" - "+ getRam() +" - "+cameraExtension;}

}
