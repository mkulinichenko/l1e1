public abstract class Phone implements PhoneConnection{
    private String name;
    private String model;
    private int storageVolume;
    private int ram;
    public Phone(String name, String model, int storageVolume, int ram) {
        this.name = name;
        this.model = model;
        this.storageVolume = storageVolume;
        this.ram = ram;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public int getStorageVolume() {
        return storageVolume;
    }
    public void setStorageVolume(int storageVolume) {
        this.storageVolume = storageVolume;
    }
    public int getRam() {
        return ram;
    }
    public void setRam(int ram) {
        this.ram = ram;
    }

    @Override
    public String toString(){return name+" - "+model+" - "+storageVolume+" - "+ram;}
}
