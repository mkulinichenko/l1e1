public class NokiaPhone extends Phone implements PhoneConnection{


    public NokiaPhone(String name, String model, int storageVolume, int ram) {
        super(name, model, storageVolume, ram);
    }

    @Override
    public void sendMessage() {
        System.out.println("Надсилання повідомлення...");
    }

    @Override
    public void call() {
        System.out.println("Дзвінок...");
    }


}
