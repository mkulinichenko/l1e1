public class Main {
    public static void main(String[] args) {
        functional(new SamsungPhone("Samsung", "J3",128,6,48));
        functional(new NokiaPhone("Nokia","3310",64,4));
        SamsungPhone samsungPhone = new SamsungPhone("SG", "A5", 256, 8, 128);
        samsungPhone.takePhoto();
        samsungPhone.takeVideo();
    }
    public static void functional(Phone phone){
        System.out.println(phone.toString());
        phone.sendMessage();
        phone.call();

    }
}
